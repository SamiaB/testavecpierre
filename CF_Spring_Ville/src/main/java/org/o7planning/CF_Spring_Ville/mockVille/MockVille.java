package org.o7planning.CF_Spring_Ville.mockVille;

import java.util.HashMap;
import java.util.Map;

import org.o7planning.CF_Spring_Ville.beans.Ville;
import org.springframework.stereotype.Component;

/**
 * Classe bouchons pour simuler la table t_ville
 * On créé des villes dans le constructeur pour que la
 * Map soit rempli à l'appel de la classe
 * @author Clément Fraisseix
 *
 */
@Component
public class MockVille {
	
	private Map<Integer, Ville> mockVille = new HashMap<Integer, Ville>();
	
	public MockVille() {		
		mockVille.put(1, new Ville(1, "Agen"));
		mockVille.put(2, new Ville(2, "Paris"));
		mockVille.put(3, new Ville(3, "Bordeaux"));
		mockVille.put(4, new Ville(4, "Toulouse"));
	}

	public Map<Integer, Ville> getMockVille() {
		return mockVille;
	}

	public void setMockVille(Map<Integer, Ville> mockVille) {
		this.mockVille = mockVille;
	}
}
