package org.o7planning.CF_Spring_Ville.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe métier Ville correspondant à la 
 * table t_ville de la BDD
 * @author Clément Fraisseix
 *
 */

@Entity
@Table(name="t_ville")
public class Ville {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	//@Basic(optional=false)
	@Column(name="idville", unique=true, nullable=false)
	private Integer idville;

	@Column(name="nomville")
	private String nomville;

	public Ville() {	
	}

	public Ville(String nom) {	
		this.nomville = nom;
	}

	public Ville(Integer code, String nom) {
		this.idville = code;
		this.nomville = nom;
	}

	public Integer getIdville() {
		return idville;
	}

	public void setIdville(Integer idville) {
		this.idville = idville;
	}

	public String getNomville() {
		return nomville;
	}

	public void setNomville(String nomville) {
		this.nomville = nomville;
	}
}

