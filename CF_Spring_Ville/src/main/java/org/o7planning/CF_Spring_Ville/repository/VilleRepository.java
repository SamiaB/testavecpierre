package org.o7planning.CF_Spring_Ville.repository;


import org.o7planning.CF_Spring_Ville.beans.Ville;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VilleRepository extends CrudRepository<Ville, Integer> {
	
}
