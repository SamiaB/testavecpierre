package org.o7planning.CF_Spring_Ville.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.o7planning.CF_Spring_Ville.beans.Ville;
import org.o7planning.CF_Spring_Ville.dao.DaoVille;
import org.o7planning.CF_Spring_Ville.mockVille.MockVille;
import org.springframework.stereotype.Repository;

/**
 * DAO Ville qui implémente l'interface
 * Méthode du CRUD récupérer, supprimer ou ajouter des données à la table t_ville
 * @author Clément Fraisseix
 *
 */
@Repository
public class DaoVilleImpl implements DaoVille<Ville> {

	MockVille mv = new MockVille();
	Map<Integer, Ville> listVille = mv.getMockVille();

	@Override
	public HashMap<Integer, Ville> findAll() {
		System.out.println("in DaoVilleImpl.findAll");
		mv.getMockVille().entrySet()
		.stream()
		.forEach(ville -> System.out.println(ville.getValue().getIdville()+" "+ville.getValue().getNomville()));
		return (HashMap<Integer, Ville>) listVille;
	}

	@Override
	public Ville findByid(Ville ville) {
		return null;
	}

	@Override
	public Ville create(Ville ville) {
		listVille = mv.getMockVille();
		listVille.put(ville.getIdville(), ville);
		return ville;
	}

	@Override
	public Ville update(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Integer id) {
		listVille = mv.getMockVille();
		listVille.remove(id);
	}
}
