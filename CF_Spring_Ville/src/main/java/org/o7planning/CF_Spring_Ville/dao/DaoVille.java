package org.o7planning.CF_Spring_Ville.dao;

import java.util.Map;

/**
 * Interface DAO générique qui sera implementé par la DAO de Ville
 * @author Clément Fraisseix
 * @param <T>
 *
 */
public interface DaoVille<T> {
	
	public Map<Integer, T> findAll();
	
	public T findByid(T ville);
	
	public T create(T ville);
	
	public T update(Integer id);
	
	public void delete(Integer id);
	
}
