package org.o7planning.CF_Spring_Ville.service;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.o7planning.CF_Spring_Ville.beans.Ville;
import org.o7planning.CF_Spring_Ville.repository.VilleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VilleService {

	@Autowired
	private Logger logger; 
	
	@Autowired
	private VilleRepository villeRepository;

	public Iterable<Ville> serviceListeVille() {
		logger.info("in VilleService.serviceListeVille");
		
		Iterable<Ville> list = villeRepository.findAll();
		list.forEach(ville -> System.out.println(ville.getIdville()+" "+ville.getNomville()));
		System.out.println("Il y a " + villeRepository.count()+" villes");

		return list;
	}
	public Optional<Ville> serviceGetVilleById(Integer id) {

		return villeRepository.findById(id);	
	}

	public Ville addVille(Ville ville) {
		System.out.println("in VilleService.addVille");
		
		villeRepository.save(ville);	
		return ville;
	}

	public void deleteVilleById(Integer id) {
		System.out.println("in VilleService.deleteVille");

		villeRepository.deleteById(id);
	}

	public VilleRepository getVilleRepository() {
		return villeRepository;
	}

	public void setVilleRepository(VilleRepository villeRepository) {
		this.villeRepository = villeRepository;
	}
}
