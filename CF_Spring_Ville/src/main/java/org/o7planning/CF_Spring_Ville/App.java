package org.o7planning.CF_Spring_Ville;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.o7planning.CF_Spring_Ville.beans.Ville;
import org.o7planning.CF_Spring_Ville.service.VilleService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

//https://www.youtube.com/watch?v=Zyqpo8gxSO0
//https://www.petrikainulainen.net/spring-data-jpa-tutorial/
//https://mkyong.com/spring-boot/spring-boot-spring-data-jpa-postgresql/
//https://www.javaguides.net/2019/07/spring-boot-save-findbyid-findall.html
//https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html
//https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ

/**
 * Lanceur de notre appli

 * Récupère le contexte et scan l'appli pour trouver les Beans Spring
 * @author Clément Fraisseix
 *
 */
//https://www.baeldung.com/the-persistence-layer-with-spring-and-jpa
@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"org.o7planning.CF_Spring_Ville"})
public class App {

	@Bean
	public Logger getLogger() {
		return LogManager.getLogger();
	}
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUsername("lab");
		dataSource.setPassword("lab123");
		dataSource.setUrl("jdbc:postgresql://localhost:5432/braderie"); 

		return dataSource;
	}

	public static void main(String[] args){

		try(AnnotationConfigApplicationContext ctx = 
				new AnnotationConfigApplicationContext(App.class)){

			VilleService serv = ctx.getBean(VilleService.class);

			//Insert une ville dans la BDD
			serv.addVille(new Ville("Agen"));
			serv.addVille(new Ville("Bordeaux"));
			serv.addVille(new Ville("Tourcoing"));

			//Afficher les villes
			serv.serviceListeVille();
		}
	}
}
